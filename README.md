# pairings2csv

Grabs pairing stats by week and exports it to a CSV file

## Prerequisites

You will need Node installed to run this script.

- Mac users can use `brew install node`
- Alternatively you can use [asdf](https://asdf-vm.com/guide/getting-started.html) to manage your Node versions.

## Installing the script

1. Clone this repository locally
1. Install the NPM dependencies

    ```shell
    npm install
    ```

1. Setup your `.env` file using the `.env.example` template:

   - `GITLAB_TOKEN` - A personal access token from GitLab.com. The token should have `api` scope and the user should have access to the Support Pairing project.
   - `GITLAB_PROJECT_ID` - The project ID of the Support Pairing project (`14978605`)
   - `GITLAB_USERNAMES` - A comma separated string of the GitLab.com usernames you want to pull pairing stats for

## Running the script

To run the script, execute the following command:

```shell
npm run start
```

The script will then generate the CSV file in the `output.csv` file.