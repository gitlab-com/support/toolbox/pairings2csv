import fetch from 'node-fetch';
import { config } from 'dotenv';
import { createObjectCsvWriter } from 'csv-writer';
import moment from 'moment';
config();

const gitlabToken = process.env.GITLAB_TOKEN;
const issuesByWeek = {};
const createdAfterDate = moment().subtract(3, 'months').format('YYYY-MM-DD');

async function fetchGitLabIssues(username, afterCursor = null) {
  const graphqlQuery = {
    query: `
      query GetIssues($username: [String!], $afterCursor: String ) {
        project(fullPath: "gitlab-com/support/support-pairing") {
          issues(assigneeUsernames: $username, state: closed, createdAfter: "${createdAfterDate}", first: 100, after: $afterCursor) {
            pageInfo {
              endCursor
              hasNextPage
            }
            edges {
              node {
                closedAt
              }
            }
          }
        }
      }
    `,
    variables: {
      username: [username],
      afterCursor: afterCursor
    }
  };

  try {
    const response = await fetch('https://gitlab.com/api/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${gitlabToken}`
      },
      body: JSON.stringify(graphqlQuery)
    });

    const data = await response.json();

    return data;
  } catch (error) {
    console.error('Error fetching GitLab issues:', error);
    throw error;
  }
}

function processIssuesData(data, username) {
  console.log('Processing issues data for', username);
  data.data.project.issues.edges.forEach(edge => {
    const closedAt = edge.node.closedAt;
    const weekStart = moment(closedAt).startOf('week').format('YYYY-MM-DD');

    if (!issuesByWeek[weekStart]) {
      issuesByWeek[weekStart] = {};
    }
    if (!issuesByWeek[weekStart][username]) {
      issuesByWeek[weekStart][username] = 0;
    }
    issuesByWeek[weekStart][username]++;
  });
}

const usernames = process.env.GITLAB_USERNAMES.split(',');

for (const username of usernames) {
  const data = await fetchGitLabIssues(username);
  processIssuesData(data, username);
}

// Convert the grouped data into a format suitable for CSV output
const records = [];
Object.entries(issuesByWeek).forEach(([weekStart, users]) => {
  Object.entries(users).forEach(([userId, count]) => {
    records.push({ userId, weekStart, issues: count });
  });
});

// Write to CSV
const csvWriter = createObjectCsvWriter({
  path: 'output.csv',
  header: [
    { id: 'userId', title: 'Name' },
    { id: 'weekStart', title: 'Week' },
    { id: 'issues', title: 'Count' }
  ]
});

csvWriter.writeRecords(records)
  .then(() => console.log('The CSV file was written successfully'));